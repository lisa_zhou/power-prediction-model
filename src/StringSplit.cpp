/*
 * StringSplit.cpp
 *
 *  Created on: May 9, 2013
 *      Author: lisa
 */
#include "StringSplit.h"

vector<string> splitString(const string& str){
    string next;

    const char& ch1 = ',';
    const char& ch2 = '(';
    const char& ch3 = ')';

    vector<string> result;

    // For each character in the string
    for (string::const_iterator it = str.begin(); it != str.end(); it++) {
        // If we've hit the terminal character
        if (*it == ch1 || *it==ch2 || *it==ch3) {
            // If we have some characters accumulated
            if (!next.empty()) {
                // Add them to the result vector
                result.push_back(next);
                next.clear();
            }
        } else {
            // Accumulate the next character into the sequence
            next += *it;
        }
    }
    if (!next.empty())
         result.push_back(next);
    return result;
}


vector<double> getParameters(string& input){
	std::vector<std::string> substr = splitString(input);

	vector<double> result;

	//retrieve intercept
	stringstream intercept_str (substr[13]);
	double intercept;
	intercept_str >> intercept;
	result.push_back(intercept);

	//retrieve slope
	stringstream slope_str (substr[14]);
	double slope;
	slope_str >> slope;
	result.push_back(slope);

	return result;
}

