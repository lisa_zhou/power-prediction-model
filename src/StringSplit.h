/*
 * StringSplit.h
 *
 *  Created on: May 9, 2013
 *      Author: lisa
 */

#ifndef STRINGSPLIT_H_
#define STRINGSPLIT_H_


#include <iostream>
#include <vector>
#include <string>
#include <sstream>
using namespace std;

vector<string> splitString(const string& str);

vector<double> getParameters(string& input);



#endif /* STRINGSPLIT_H_ */
