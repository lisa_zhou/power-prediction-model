x<-array(c(1.5,2.4,3.2,4.8,5.0,7.0,8.43),dim=c(7,1))

y<-array(c(3.5,5.3,7.7,6.2,11.0,9.5,10.27),dim=c(7,1))

dummy<-data.frame(x=x,y=y)

fm<-lm(y~x,data=dummy)

summary(fm)