/*
 * LinearRegression.cpp
 *
 *  Created on: May 9, 2013
 *      Author: lisa
 */

#include "LinearRegression.h"

std::string computeLinearRegression(RInside R, NumericVector x, NumericVector y){

	//create (x,y) pairs
	R["x"] = x;
	R["y"] = y;

	List dummy = List::create(x,y);
	R["dummy"] = dummy;

	//linear regression
	std::string expr = "fm<-lm(y~x,data=dummy)";
	R.parseEval(expr);

	List summary = R.parseEval("summary(fm)");
	R["summary"]=summary;

	std::string result = R.parseEval("toString(summary)");

	return result;
}


