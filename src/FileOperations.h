/*
 * FileOperations.h
 *
 *  Created on: May 27, 2013
 *      Author: lisa
 */

#ifndef FILEOPERATIONS_H_
#define FILEOPERATIONS_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstring>
#include <algorithm>
#include <sstream>
using namespace std;


vector<string> split(string& str,const char* c);

double convertString2Double(string& integer, string& fraction);

vector<double> retrieveXVector(ifstream& file);

#endif /* FILEOPERATIONS_H_ */
