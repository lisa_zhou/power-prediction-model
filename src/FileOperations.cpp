/*
 * FileOperations.cpp
 *
 *  Created on: May 27, 2013
 *      Author: lisa
 */

#include "FileOperations.h"

vector<string> split(string& str,const char* c)
{
    char *cstr, *p;
    vector<string> res;
    cstr = new char[str.size()+1];
    strcpy(cstr,str.c_str());
    p = strtok(cstr,c);
    while(p!=NULL)
    {
        res.push_back(p);
        p = strtok(NULL,c);
    }
    return res;
}

double convertString2Double(string& integer, string& fraction){
	stringstream ss;
	ss << integer << "." << fraction;
	return atof(ss.str().c_str());
}

vector<double> retrieveXVector(ifstream& file){
	double cycles, instructions;
	  vector<double> ratio;
	  vector<string> substr;

	  string line;
	  const char delimiter = ',';

	  if (file.is_open())
	  {
		  //skip the header
		  getline (file,line);
		  while ( file.good() )
		  {
			  getline (file,line);
			  replace(line.begin(), line.end(), '%', ' ');
			  substr = split(line,&delimiter);

			  if(substr.size() == 6){
				  cycles = convertString2Double(substr[2], substr[3]);
				  instructions = convertString2Double(substr[4], substr[5]);
				  ratio.push_back(cycles/instructions);
			  }
	      }
		  substr.clear();
	  }
	  return ratio;
}


