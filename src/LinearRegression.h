/*
 * LinearRegression.h
 *
 *  Created on: May 9, 2013
 *      Author: lisa
 */

#ifndef LINEARREGRESSION_H_
#define LINEARREGRESSION_H_

#include <Rcpp.h>
#include <RInside.h>
using namespace Rcpp;

std::string computeLinearRegression(RInside R, NumericVector x, NumericVector y);



#endif /* LINEARREGRESSION_H_ */
