/*
 * main.cpp
 *
 *  Created on: May 6, 2013
 *      Author: lisa
 */
#include <Rcpp.h>
#include <RInside.h>                    // for the embedded R via RInside
#include <vector>
#include <fstream>
#include "StringSplit.h"
#include "LinearRegression.h"
#include "FileOperations.h"
using namespace Rcpp;

bool ablePrint(){
	bool printable = true;
	return printable;
}

int main(int argc, char *argv[]) {

	//PMC input
	ifstream myfile ("/home/lisa/Desktop/libpfm-4.3.0/Energy/FE_20130526_14:21:34:9288.txt");

	vector<double> ratio;
//	ratio = retrieveXVector(myfile);
	ratio.push_back(1.5);
	ratio.push_back(2.4);
	ratio.push_back(3.2);
	ratio.push_back(4.8);
	ratio.push_back(5.0);
	ratio.push_back(7.0);
	ratio.push_back(8.43);

	//True Power input
	vector<double> power;
	power.push_back(3.5);
	power.push_back(5.3);
	power.push_back(7.7);
	power.push_back(6.2);
	power.push_back(11.0);
	power.push_back(9.5);
	power.push_back(10.27);

	if(ratio.size() != power.size()){
		std::cout << "power and ratio are NOT the same size!" << endl;
		return 0;
	}

	//take two inputs to linear regression
  RInside R(argc, argv);

//  NumericVector x = NumericVector::create(1.5,2.4,3.2,4.8,5.0,7.0,8.43);
//  NumericVector y = NumericVector::create(3.5,5.3,7.7,6.2,11.0,9.5,10.27);

  NumericVector x = Rcpp::wrap(ratio);
  NumericVector y = Rcpp::wrap(power);


  std::string result = computeLinearRegression(R, x, y);

  std::vector<double> parameters = getParameters(result);

  if(ablePrint()){
	  std::cout<<"****print the formula***********"<<std::endl;
	  std::cout<<"y = "<<parameters[1]<<"*x + "<<parameters[0]<<endl;
  }



  return 0;
}

/*
 * Call:
lm(formula = y ~ x, data = dummy)

Residuals:
      1       2       3       4       5       6       7
-1.3185 -0.3324  1.3442 -1.6026  3.0165 -0.2920 -0.8151

Coefficients:
            Estimate Std. Error t value Pr(>|t|)
(Intercept)   3.4621     1.5201   2.278   0.0718 .
x             0.9043     0.2946   3.069   0.0278 *
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 1.793 on 5 degrees of freedom
Multiple R-squared:  0.6533,	Adjusted R-squared:  0.5839
F-statistic: 9.421 on 1 and 5 DF,  p-value: 0.0278
 *
 */

